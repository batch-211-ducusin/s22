console.log("Hello World");

//JS Array Manipulation.

//Array Methods
	/*
		JS has built-in functions and methods for arrays. This allows us to manipulate and access array items
	*/


//Mutator Methods
	/*
		- Mutator Methods are functions that "mutate" or change an array after they're created.
		- These methods manipulate the original array performing various tasks such as adding and removing elements.
	*/
	let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

	
	//push()
	/*
		push() adds an element in the end of an array AND returns the array's length.

		Syntax:
			arrayName.push();
	*/
	console.log("Current Array: ");
	console.log(fruits);	//log fruits array

	let fruitsLength = fruits.push("Mango");
	console.log(fruitsLength);	//5
	console.log("push() Method");
	console.log(fruits);	//result nya, nadagdag na si "Mango".

	fruits.push("Avocado", "Guava");
	console.log("push() Method 2");
	console.log(fruits);	//result nya, nadagdag na si "Avocado" at "Guava".


	//pop()
	/*
		removes the last element in an array AND  returns the removed element.

		Syntax:
			arrayName.pop()
	*/
	let removeFruit = fruits.pop();
	console.log(removeFruit);	//result nya, pinirint sa console ang tinangal ng last element ng array.
	console.log("pop() Method");
	console.log(fruits);	//guava is removed



	//Mini Activity
	/*
		create a function which will unfriend the last person in the array.
		log the ghostFighters array in our console.
	*/
	let ghostFighters = ["eugine", "Dennis", "Alfred", "Taguro"];
	function removeTaguro(){
		ghostFighters.pop();
	};
	removeTaguro();
	console.log(ghostFighters);



	//unshift()
	/*
		adds one or more elements at the beginning of an array.

		Syntax:
			arrayName.unshift("elementA");
			arrayName.unshift("elementA", "elementB");
	*/
	fruits.unshift("Lime", "Banana");
	console.log("unshift() Method");
	console.log(fruits);	//result, nadagdag si "Lime", "Banana" sa unahan ng array.


	//shift()
	/*
		removes an element at the beginning of an array AND returns the removed element.

		Syntax:
			arrayName.shift();
	*/
	let anotherFruit = fruits.shift();
	console.log("shift() Method");
	console.log(anotherFruit);	//"Lime"
	console.log(fruits);	//natangal na si lime sa array.


	//splice()
	/*
		simultaneously removes elements from a specified index number and adds elements.

		Syntax:
			arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
	*/
	fruits.splice(1, 2, "Lime", "Cherry");
	console.log("splice() Method");
	console.log(fruits);	//result, nadagdag na sila "Lime" and "Cherry" at natangal sila "apple" at "orange".



	//sort()
	/*
		Rearranges the array elements in alphanumeric order.

		Syntax:
			arrayName.sort();
	*/
	fruits.sort();
	console.log("sort() Method");
	console.log(fruits);



	//reverse()
	/*
		reverses the order of array elements.

		Syntax:
			arrayName.reverse();
	*/
	fruits.reverse();
	console.log("reverse() Method");
	console.log(fruits);





//Non-Mutator Methods
	/*
		Non-Mutator methods are function that do not modify or change an array after they are created.

		- these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing th ourput.
	*/

	let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];


	//indexOf()
	/*
		returns the index number of the first matching element found in an array.
		- if no match is found, the result is -1.
		- the search process will be done from first element proceeding to the last element.

		Syntax:
			arrayName.indexOf(searchValue);
			arrayName.indexOf(searchValue, fromIndex); 
	*/
	let firstIndex = countries.indexOf("PH");
	console.log("indexOf() Method");
	console.log(firstIndex);

	//result kapag nag hanap ng wala.
	let invalidCountry = countries.indexOf("BR");
	console.log("indexOf() Method 2");
	console.log(invalidCountry);	//-1


	//lastIndexOf()
	/*
		returns the index number of the last matching element found in an array.
		- the search process will be done from the last element proceeding to the first element.

		Syntax:
			arrayName.lastIndexOf(searchValue);
			arrayName.lastIndexOf(searchValue, fromIndex);
	*/
	let lastIndex = countries.lastIndexOf("PH");
	console.log("lastIndexOf() Method");
	console.log(lastIndex);	//5


	//slice()
	/*
		- portion or slice elements from an array AND returns a new array

		Syntax:
			arrayName.slice(startingIndex);
			arrayName.slice(startingIndex, endindIndex);
	*/
	let slicedArrayA = countries.slice(2);
	console.log("slice() Method 1");
	console.log(slicedArrayA);

	let slicedArrayB = countries.slice(2, 4);
	console.log("slice() Method 2");
	console.log(slicedArrayB);

	let slicedArrayC = countries.slice(-3);
	console.log("slice() Method 3");
	console.log(slicedArrayC);


	//toString()
	/*
		it turns our array into a string
		returns an array as a string separated by commas.

		Syntax:
			arrayName.toString();
	*/
	let stringArray = countries.toString();
	console.log(" toString() Method ");
	console.log(stringArray);


	//concat()
	/*
		combines two arrays or more and returns the combined result.

		Syntax:
			arrayA.concat(arrayB);
			arrayA.concat(elementA);
	*/
	let taskArrayA = ["drink hmtl", "eat javascript"];
	let taskArrayB = ["inhale css", "breath sass"];
	let taskArrayC = ["get git", "be node"];

	let task = taskArrayA.concat(taskArrayB);
	console.log("concat() Method 1");
	console.log(task);


	//Combine Multiple Arrays
	console.log("concat() Method 2");
	let allTask = taskArrayA.concat(taskArrayB, taskArrayC);
	console.log(allTask);	//result mag kakacombine na silang lahat.

	//Combining Arrays with Elements
	let combinedTask = taskArrayA.concat("smell express", "throw react");
	console.log("concat() Method 3");
	console.log(combinedTask);


	//join()
	/*
		- Returns an array as a string separeted by specified separator.

		Syntax:
			arrayName.join("separatorString");
	*/
	let users = ["John", "Jane", "Joe", "Robert", "Nej"];
	
	console.log(users.join());
	console.log(users.join(" "));
	console.log(users.join(" - "));




//Iteration Method
	/*
		- Iteration methods are loops designed to perform repetitive task on arrays.
		- Iteration methods loops over all items in an array.
		- Useful for manipulating array data resulting in complex tasks.
	*/


	//forEach()
	/*
		- Similar to a for loop that iterates on each array element.
		- For each item in the array, the anonymous function passed in the forEach() method will be run.
		- The anonymous function is able to recieve the current item beign iterated or loop over by assigning a parameter.
		- Variable names for arrays are normally written in the plural form of the data stored in an array.
		- It's common practice to use the singular form of the array content for paramenter names used in array loops.
		- forEach() does not return anything.

		Sytax:
			arrayName.forEach(function(indivElement)){
				statement
			}
	*/
	allTask.forEach(function(task){
		console.log(task);
	})



	//Mini Activity
	/*
		- create a function that can display the dragonBall one by one in our console.
		- invoke the function
	*/
	let dragonBall = ["Goku", "Gohan", "Mr. Pogi", "Vegeta"];
	function displayDragon(){
		dragonBall.forEach(function(char){
		console.log(char);
		})
	};
	displayDragon();



	//Using forEach with conditional statements

	//Looping through all arrays items
	/*
		it is a good practice to print the current element in the console when working with array iteration method to have an idea of what information is being worked on for each iteration of the loop.

		creating a separate variable to sotre result of an array iteration iteration methods are also good practice to avoid confusion by modifiying the original array.

		mastering loops and arrays allow us developers to perform.
	*/
	let filteredTasks = [];

	allTask.forEach(function(task){
		if(task.length > 10){
			filteredTasks.push(task)
		}
	});
	console.log("Result of filtered tasks: ");
	console.log(filteredTasks);


	//map()
	/*
		- iterates on each element AND returns new array with different values depending on the result of the function's operation.

		- map requires an return code.

		Syntax:
			let resultArray = arrayName.map(function(individualElement){
	
			})
	*/
	let numbers = [1, 2, 3, 4, 5];

	let numberMap = numbers.map(function(ber){
		return ber * ber;
	})
	console.log("original array");
	console.log(numbers);
	console.log("result of map method");
	console.log(numberMap);


	//map() VS forEach()
	let numberForEach = numbers.forEach(function(ber){
		return ber * ber;
	})
	console.log(numberForEach);


	//every()
	/*
		- checks if all elements in an array meet the given conditions.
		- this is useful for validating data stored in arrays especially when dealing with large amounts of data.
		- returns true if all elements meet the condition, otherwise false.

		Syntax:
			let resultArray = arrayName.every()
	*/
	let allValid = numbers.every(function(er){
		return (er < 3);
	})
	console.log("Result of every method: ");
	console.log(allValid)


	//some()
	/*
		- Check if at least one lement in the array meet the given condition.
		- Return a true value if at least one element meets the condition and false if otherwise.

		Syntax:
			let resultArray = arrayName.some(function(indivElement){
				return expression or condition;
			})
	*/
	let someValid = numbers.some(function(r){
		return (r < 2);
	});
	console.log("result of some method");
	console.log(someValid);

	//Combining the returned result from every or some method may be used in other statements to perform consecutive results.
	if(someValid){
		console.log("some numbers in the array are greater than 2");
	}


	//filter
	/*
		- returns a new array that contains which meets a given condition.
		- returns an empty array if no elements were found.

		Syntax:
			let resultArray = arrayName.filter(function(indiviElement){
				return condition
			})
	*/
	let filterValid = numbers.filter(function(b){
		return(b < 3)
	})
	console.log("Result of filter method: ");
	console.log(filterValid);

	let nothingFound = numbers.filter(function(a){
		return (a = 0);
	})
	console.log("result of filter method 0");
	console.log(nothingFound);


	//Filtering using forEach()
	/*
		
	*/
	let filteredNumbers = [];
	numbers.forEach(function(u){
		// console.log(u);
		if(u < 3){
			filteredNumbers.push(u);
		}
	});
	console.log("result of filter method: ");
	console.log(filteredNumbers);


	//Includes()
	/*
		- Check if the argument passed can be found in the array.
		- it returns a boolean which can be saved in a variable.
		- returns true if the argument is found the array.
		- returns false if it is not.

		Syntax:
			arrayName.includes(<argumentToFind>);
	*/
	let products = ["mouse", "keyboard", "laptop", "monitor"];
	let productFound = products.includes("mouse");
	console.log(productFound);	//true

	let productNotFound = products.includes("Headset");
	console.log(productNotFound);	//false




	//Method Chaining
	/*
		- Ang mga methods ay puwedeng i-chain, o gamitin ng sunod-sunod. (look example below)
		- The result of the first method is used on the second  method until all "chained" methods have been resolved.
	*/
	let filteredProducts = products.filter(function(pro){
		return pro.toLowerCase().includes("a");
	});
	console.log(filteredProducts);




	//Mini Activity
	/*
		create an addTrainer function that will enable us to add a trainer in the contacts array.
		- this function should be able to recive a string
		- determine if the added trainer already exists in the contacts array:
		- if it is, show an alert saying "Already added in the Match Call"
		- if it is not, add the trainer in the contacts array and show an alert saying "registered!"
		- invoke and add a trainer in the brower's console.
		- in the console, log the contacts array.
	*/
	let contacts = ["Ash"];

/*	function addTrainer(trainer){
		if(contacts.includes(trainer) == true){
			console.log("Already added in the Match Call");
		}
	};
	addTrainer("Ash")*/

	function addTrainer(trainer){
		let doesTrainerExist = contacts.includes(trainer);

		if(doesTrainerExist){
			alert("Already added in the Match Call");
		} else {
			contacts.push(trainer);
			alert("Registered!");
		}
	};
	addTrainer("Brok");
	console.log("Mini Activity");
	console.log(contacts);



	//reduce
	/*
		-evaluates elements from left to right and returns or reduce the array into a single value.

		Syntax:
			let resultArray = arrayName.reduce(function(accumulator, currentValue){
				reurn expression or operation
			})
				- "Accumulator" parameter in the function stores the result for every iteration of the loop.
				="currentValue" is the current or next element in the array that is evaluated in each iteration of the loop.
	*/
	console.log(numbers);
	let iteration = 0;
	let iterationStr = 0;

	let reduceArray = numbers.reduce(function(x, y){
		console.warn("Current iteration: " + ++iteration);
		console.log("accumulator: " + x);
		console.log("current value: " + y);

		return x + y;
	});

	console.log("Result fo reduce method: " + reduceArray);


	let list = ["Hello", "Again", "World"];

	let reducedJoin = list.reduce(function(x,y){
		console.warn('current iteration: ' + ++iterationStr);
		console.log('accumulator: ' + x);
		console.log('current value: ' + y);

		return x + ' ' + y;
	})
	console.log("Result of reduce method: " + reducedJoin);
